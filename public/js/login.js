// Realiza a validação dos dados fornecidos
// TODO: Pensar em como tratar o caso de nick já existente

$(function() {
  $('#login-form').submit(function() {
    var nome = $('#nome').val();
    var canal = $('#canal').val();
    var servidor = $('#servidor').val();

    if (!(nome && canal && servidor)) {
      alert('Preencha todos os campos!');
      return false;
    }

    return true; // Envia para o servidor
  });
});

function mudarServidor(server, canal) {
  $('#servidor').val(server);

  if (!$('#canal').val())
    $('#canal').val(canal);
}
