# Sistemas Distribuídos Projeto 2: Cliente IRC Web

## Membros

* Alvaro Rodrigues
* Felipe Saraiva
* Victor Carvalho

## Wiki

Todas as informações do projeto, assim como a lista de implementações, o andamento do projeto e procedimentos úteis estão localizados na wiki do GitLab.  
<https://gitlab.com/sd1-ec-2017-2/p2-g3/wikis/home>
